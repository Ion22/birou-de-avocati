function tabShowContent(str) {
	var elementContent1 = document.getElementById("tab-admin");
	var elementContent2 = document.getElementById("tab-client");
	var elementContent3 = document.getElementById("tab-avocat");


	var arr = [
		elementContent1,
		elementContent2,
		elementContent3
	];

	for (let i = 0; i < arr.length; i++) {

		if (str === arr[i].id) {
			console.log(arr[i]);
			arr[i].classList.add("tab-active");
			for (let j = 0; j < arr.length; j++) {
				if (arr[i] !== arr[j]) {
					arr[j].classList.remove("tab-active");
				}
			}

		}
	}

	// var element = document.getElementsByTagName("button");
	// element[0].add("tab-active");
}

document.addEventListener('DOMContentLoaded', () => {

	let myBtns = document.querySelectorAll('.tab-btn');
	myBtns.forEach(function (btn) {

		btn.addEventListener('click', () => {
			myBtns.forEach(b => b.classList.remove('btn-active'));
			btn.classList.add('btn-active');
		});

	});

});